use ModernWays;

alter table huisdieren add column Geluid varchar(20) charset utf8mb4;

SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET geluid = 'WAF!' where soort = 'hond';
update huisdieren set geluid='miauwww' where soort = 'kat' ;
SET SQL_SAFE_UPDATES = 1;