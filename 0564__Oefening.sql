use ModernWays;
drop table Studenten, Opleiding, Vak, Lector;
create table if not exists Studenten(
Studentnummer int auto_increment primary key,
Voornaam varchar(100) char set utf8mb4 not null,
Familienaam varchar(100) char set utf8mb4 not null
);
create table if not exists Opleiding(
Naam varchar(100) char set utf8mb4 primary key 
);
create table if not exists Vak(
Naam varchar(100) char set utf8mb4 primary key
);
create table if not exists Lector(
Personeelsnummer int auto_increment primary key,
Voornaam varchar(100) char set utf8mb4 not null,
Familienaam varchar(100) char set utf8mb4 not null
);
alter table Studenten 
add column Opleiding_Naam varchar(100) char set utf8mb4 not null,
add column Semester tinyint unsigned not null,
add constraint fk_Studenten_Opleiding
foreign key (Opleiding_Naam)
references Opleiding(Naam);

create table LectorGeeftVak(
Lector_Personeelsnummer int not null,
Vak_Naam varchar(100) char set utf8mb4,
constraint fk_LectorGeeftVak_Lector
foreign key (Lector_Personeelsnummer)
references Lector(Personeelsnummer),
constraint fk_LectorGeeftVak_Vak
foreign key (Vak_Naam)
references Vak(naam)
);






