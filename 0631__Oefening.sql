use ModernWays;
select distinct Voornaam 
from Studenten
where Voornaam in (select distinct Voornaam from Personeelsleden) and
Voornaam in (select distinct Voornaam from Directieleden);