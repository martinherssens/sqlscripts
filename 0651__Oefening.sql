USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration` (in albumId int, out totalDuration smallint unsigned )
BEGIN
declare ok int default 0;
declare songDuration tinyint unsigned;

declare currentSong
cursor for select Lengte from Liedjes where Albums_Id = albumId;
declare continue handler for not found set ok = 1;
set totalDuration = 0;
open currentSong;
getSong: loop
	fetch currentSong into songDuration;
    if ok = 1 then 
    leave getSong;
    end if;
    set totalDuration = totalDuration + songDuration;
    end loop getSong;
close currentSong;
END$$

DELIMITER ;
