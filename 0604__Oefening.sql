use ModernWays;
create table Uitleningen (
Startdatum date not null,
Einddatum date null,
Leden_Id INT not null,
Boeken_Id int not null,
constraint fk_Uitleningen_Leden
foreign key (Leden_Id)
references Leden(Id),
constraint fk_Uitleningen_Boeken
foreign key (Boeken_Id)
references Boeken(Id));
