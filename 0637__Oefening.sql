use ModernWays;
-- eerst een tabel personen aanmaken met voornaam en familienaam
create table Personen(
Voornaam varchar(255) not null,
Familienaam varchar(255) not null
);

-- tabel personen invullen met de gegevens uit de tabel Boeken
insert into Personen(Voornaam,Familienaam)
select distinct Voornaam,Familienaam from Boeken;
 
-- meerdere kolommen toevoegen aan tabel personen
alter table Personen add(
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);

-- testen of we de volledige tabel hebben
select * from Personen order by Familienaam, Voornaam;

-- foreign key aanmaken in de tabel Boeken om deze te linken aan de tabel Personen
alter table Boeken add(
Personen_Id int not null);
 
-- nu willen we de foreign key in de tabel boeken invullen, hiervoor leggen we een relatie tussen deze 2 tabellen
set sql_safe_updates = 0;
update Boeken cross join Personen
set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;

-- testen of kolom Personen_Id is ingevuld
select Voornaam,Familienaam,Personen_Id from Boeken;

-- uiteraard zijn de kolom Voornaam en Familienaam uit de tabel Boeken nu overbodig, deze verwijderen we
alter table Boeken drop column Voornaam,
drop column Familienaam;

-- nu gaan we de foreign key constraint toevoegen op de kolom Personen_Id
alter table Boeken
add constraint fk_Boeken_Personen
foreign key (Personen_Id)
references Personen(Id);

-- Tabel genormaliseerd

