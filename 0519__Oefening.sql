use ModernWays;

alter table liedjes add column genre varchar(20) charset utf8mb4;

SET SQL_SAFE_UPDATES = 0;
UPDATE liedjes SET genre = 'hard rock' where artiest in ('Led Zeppelin', 'Van Halen');
SET SQL_SAFE_UPDATES = 1;