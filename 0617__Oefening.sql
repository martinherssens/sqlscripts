use ModernWays;
create view AuteursBoekenRatings as
select AuteursBoeken.Auteur, AuteursBoeken.Titel, GemiddeldeRatings.Rating
from AuteursBoeken
inner join GemiddeldeRatings
on GemiddeldeRatings.Boeken_Id = AuteursBoeken.Boeken_Id;