use ModernWays;

SELECT Games.Titel, Platformen.Naam
FROM
Games LEFT JOIN -- de “tweede” JOIN
-- haakjes worden altijd eerst uitgewerkt, gebruik ze ter verduidelijking!
(Platformen INNER JOIN Releases ON Releases.Platformen_Id = Platformen.Id)
ON Releases.Games_Id = Games.Id;