use ModernWays;
Select Voornaam, coalesce (Omschrijving, 'Geen omschrijving') as 'Omschrijving'
from Taken
Left join Leden
on Taken.Leden_Id = Leden.Id
where Leden_Id is null;