USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(out Aantal tinyint)
BEGIN
select count(*)
into Aantal
from Genres;

END$$

DELIMITER ;