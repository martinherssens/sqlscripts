USE ModernWays;

CREATE table Metingen(
Tijdstip DATETIME NOT NULL,
Grootte SMALLINT UNSIGNED NOT NULL,
Marge FLOAT(3,2))
