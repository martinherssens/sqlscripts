USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `DemonstrateHandlerOrder`()
BEGIN
declare randomGetal tinyint default 0;

declare continue handler for sqlstate '45002' 
begin
	select "State 45002 opgevangen. Geen probleem." as message;
end;
declare continue handler for sqlexception 
begin
	select 'Een algemene fout opgevangen';
end;

set randomGetal = floor(rand()*3)+1;
if randomGetal = 1 then
signal sqlstate '45001';
elseif randomGetal = 2 then 
signal sqlstate '45002';
else 
signal sqlstate '45003';
end if;

END$$

DELIMITER ;