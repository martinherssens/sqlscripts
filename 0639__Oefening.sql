use ModernWays;
insert into Personen(Voornaam, Familienaam)
values ('Jean-Paul','Sartre');

insert into Boeken(Titel,Uitgeverij,Verschijningsdatum,Personen_Id)
values ('De woorden','De Bezige Bij','1961',(select Id from Personen where Familienaam = 'Sartre' and Voornaam = 'Jean-Paul'));