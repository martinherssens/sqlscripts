USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleasesLoop`(in extraReleases int )
BEGIN
declare counter int default 0;
declare success bool;
callloop: loop
	call MockAlbumReleaseWithSuccess(success);
    if success = 1 then set counter = counter +1;
    end if;
    if counter = extraReleases then 
		leave callloop;
    end if;
end loop;

END$$

DELIMITER ;