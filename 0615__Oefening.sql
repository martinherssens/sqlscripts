use ModernWays;
Alter view AuteursBoeken 
as
select concat(Personen.voornaam, " ",Personen.Familienaam) as "Auteur", Boeken.Titel, Boeken.Id as "Boeken_Id"
from Boeken
inner join Publicaties on Boeken.Id = Boeken_Id
inner join Personen on Personen.Id = Publicaties.Personen_Id;