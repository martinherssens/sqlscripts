USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleaseWithSuccess`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `MockAlbumReleaseWithSuccess`(out success bool)
BEGIN
declare numberOfAlbums, numberOfBands, randomAlbumId, randomBandId int default 0;
select count(*) into numberOfAlbums from Albums;
select count(*) into numberOfBands from Bands;

set randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
set randomBandId = FLOOR(RAND() * numberOfBands) +1;
IF  ((select Bands_Id from AlbumReleases where Bands_Id = randomBandId) is null AND 
        (select Albums_Id from AlbumReleases where Albums_Id = randomAlbumId) is null)
	THEN
    insert into AlbumReleases ( Bands_Id, Albums_Id)
    values (randomBandId, randomAlbumId);
    set success = 1;
    else 
    set success = 0;
end if;

END$$

DELIMITER ;
