use ModernWays;
select games.Titel, coalesce( releases.Games_Id, 'geen platformen gekend' ) as Platformen
from Games
left join Releases
on Games_Id = Games.Id
where Games_Id is null
UNION 
select coalesce(releases.Platformen_Id, 'geen games gekend') , platformen.Naam
from Platformen
left join Releases
on Platformen_Id = Platformen.Id
where Platformen_Id is null;


