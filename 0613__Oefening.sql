use ModernWays;
create view AuteursBoeken
as
select concat(Personen.Voornaam," ",Personen.Familienaam)as "Auteur",Boeken.Titel
from Publicaties
inner join Personen on Personen_Id = Personen.Id
inner join Boeken on Boeken_Id = Boeken.Id;